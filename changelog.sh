#!/bin/bash

file=""
array=($(git tag -l))
length=${#array[@]}
tags=""

if [ $length -gt 1 ]
then
    tags="${array[length-2]}..${array[length-1]}"
elif [ $length -gt 0 ]
then
    tags="${array[length-1]}"
fi

curl https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases --request POST --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $TOKEN_GITLAB" --data @<(cat <<EOF
{
    "id": "$CI_PROJECT_ID",
    "name": "Release $CI_COMMIT_TAG",
    "tag_name": "$CI_COMMIT_TAG",
    "description": "Apk versión $CI_COMMIT_TAG",
    "assets": {
        "links": [
            {
                "name": "app-release.apk",
                "url": "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/artifacts/$CI_COMMIT_TAG/raw/dev/src-cordova/platforms/android/app/build/outputs/apk/release/app-release.apk?job=build"
            }
        ]
    }
}
EOF
)
