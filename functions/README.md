# Funciones de firebase

El código para ejecutar una funcion de firebase es el siguiente:

```js
import firebase from "firebase/app";
require("firebase/auth");
require("firebase/functions");

const app = firebase.initializeApp({
  // ...
});

const sendEmail = app.functions().httpsCallable("sendEmail");
sendEmail({ invitation: INVITATION })
  .then(console.log)
  .catch(console.error);
```

El valor de `INVITATION` es el identificador del documento de invitation de firestore ejemplo **miuaUs5MODp5pO3UwLH7**, ademas el campo de `accepted` del request debe ser **1** ó **true**.

![](firestore_invitation.png)
![](firestore.png)

Una vez ejecutada la función va enviar un correo para que imprima su invitación siembre y cuando exista su identificador y el campo accepted sea igual a **1** ó **true**.

Dentro de la invitación viene un código QR con el identificador de la INVITACIÓN.
