const functions = require("firebase-functions");
const admin = require("firebase-admin");
const mailgun = require("mailgun.js");
const QRCode = require("qrcode");
const moment = require("moment-timezone");
const fs = require("fs");

admin.initializeApp({
  apiKey: "AIzaSyAl6nskuWYu91ecZ_uCpr_z5Rj_DEvtAlU",
  authDomain: "moviles2020-e9628.firebaseapp.com",
  databaseURL: "https://moviles2020-e9628.firebaseio.com",
  projectId: "moviles2020-e9628",
  storageBucket: "moviles2020-e9628.appspot.com",
  messagingSenderId: "577098757874",
  appId: "1:577098757874:web:dbca580be5cdfcbda5026b",
  measurementId: "G-QPVXBGCKCJ"
});

exports.sendEmail = functions.https.onCall((data, context) => {
  const invitation = data.invitation;
  const mg = mailgun.client({
    username: "api",
    key: functions.config().mailgun.api_key,
    public_key: functions.config().mailgun.public_key
  });
  const html = fs.readFileSync("template/mail.html", { encoding: "utf-8" });

  return admin
    .firestore()
    .collection("invitations")
    .doc(invitation)
    .get()
    .then(doc => {
      if (!doc.exists) {
        throw new functions.https.HttpsError(
          "invalid_argument",
          `No existe ningún registro con la Invitación: "${invitation}"`
        );
      }

      return doc.data().request.get();
    })
    .then(doc => {
      if (!doc.exists) {
        throw new functions.https.HttpsError(
          "invalid_argument",
          `No existe ningún *Request* con el registro de la Invitación: "${invitation}"`
        );
      }

      const info = doc.data();
      if (!info.accepted) {
        throw new functions.https.HttpsError(
          "aborted",
          `No ha sido aceptada la invitación`
        );
      }

      const data = {
        from: `Invitación <me@${functions.config().mailgun.domain}>`,
        to: [info.email],
        subject: "Invitación",
        html: html
          .replace(
            "%DISPLAY_NAME%",
            `${info.name}`
              .split(" ")
              .map(i => `${i[0].toUpperCase()}${i.substring(1)}`)
              .join(" ")
          )
          .replace(
            /\%LINK\%/g,
            `https://us-central1-moviles2020-e9628.cloudfunctions.net/invitation?ref=${invitation}`
          )
      };
      return mg.messages.create(functions.config().mailgun.domain, data);
    })
    .then(msg => {
      return "El correo fue enviado.";
    });
});

exports.invitation = functions.https.onRequest(async (req, res) => {
  var refInvitation = null;
  const data = {};
  const invitation = req.query.ref;
  const html = fs.readFileSync("template/index.html", { encoding: "utf-8" });

  QRCode.toDataURL(invitation)
    .then(QRInBase64 => {
      data.qr = QRInBase64;
      return admin
        .firestore()
        .collection("invitations")
        .doc(invitation)
        .get();
    })
    .then(doc => {
      if (!doc.exists) {
        throw new Error(
          `No existe ningún registro con la Invitación: "${invitation}"`
        );
      }

      refInvitation = doc.data();
      return refInvitation.request.get();
    })
    .then(doc => {
      if (!doc.exists) {
        throw new Error(
          `No existe ningún registro con la Invitación: "${invitation}"`
        );
      }

      const info = doc.data();
      if (!info.accepted) {
        throw new Error(
          `No ha sido aceptada la invitación con la Invitación: "${invitation}"`
        );
      }

      data.user = {
        name: info.name,
        last_name: info.lastname,
        start_year: info.start_year,
        end_year: info.end_year
      };

      return info.career.get();
    })
    .then(doc => {
      if (!doc.exists) {
        throw new Error(
          `No existe ningún carrera con el registro de la Invitación: "${invitation}"`
        );
      }

      data.user.career = doc.data().name;
      return refInvitation.event.get();
    })
    .then(doc => {
      if (!doc.exists) {
        throw new Error(
          `No existe ningún evento con el registro de la Invitación: "${invitation}"`
        );
      }

      const event = doc.data();
      moment.locale("es");

      data.event = {
        date: moment(event.date._seconds, "X")
          .tz("America/Mexico_City")
          .format("LL h:mm a"),
        program: event.program
      };
      res.send(html.replace("%DATA%", JSON.stringify(data)));
    })
    .catch(err => {
      console.error(err);
      res.send("Opps hubo un error. Intente mas tarde");
    });
});
