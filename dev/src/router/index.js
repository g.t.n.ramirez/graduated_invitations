import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
import Home from "../views/Home.vue";
import Register from "@/views/Register";
import Inv from "@/views/Invitation";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Register",
    component: Register
  },
  {
    path: "/finalinv",
    name: "FinalInv",
    component: Inv
  },
  {
    path: "/home",
    name: "Home",
    component: Home
  },
  {
    path: "/login",
    name: "Login",
    component: Login
  }
];

var configRouter = {
  routes: routes
};

if (!process.env.CORDOVA_PLATFORM) {
  configRouter = {
    ...configRouter,
    mode: "history",
    base: process.env.BASE_URL
  };
}

export default new VueRouter(configRouter);
