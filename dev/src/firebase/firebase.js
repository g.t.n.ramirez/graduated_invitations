import firebase from 'firebase/app'
require("firebase/auth");
require("firebase/functions");

//import firestore from 'firebase/firestore'

  const firebaseConfig = {
    apiKey: "AIzaSyAl6nskuWYu91ecZ_uCpr_z5Rj_DEvtAlU",
    authDomain: "moviles2020-e9628.firebaseapp.com",
    databaseURL: "https://moviles2020-e9628.firebaseio.com",
    projectId: "moviles2020-e9628",
    storageBucket: "moviles2020-e9628.appspot.com",
    messagingSenderId: "577098757874",
    appId: "1:577098757874:web:dbca580be5cdfcbda5026b",
    measurementId: "G-QPVXBGCKCJ"
  };
  
  const firebaseApp = firebase.initializeApp(firebaseConfig);
  firebaseApp.firestore().settings({ timestampsInSnapshots: true })
  //const firebase = require("firebase");
  // Required for side-effects
  require("firebase/firestore");
  export default firebaseApp;