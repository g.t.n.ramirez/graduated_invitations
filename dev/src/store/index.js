import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: '',
    carreers: [],
    list: []
  },
  mutations: {
    login(state, item) {
      state.user = item
    },
    pushCarreer(state, item) {
      state.carreers = item;
    },
    setList(state, item){
      state.list = item;
    }
  },
  actions: {
    pushCarreerAsync({
      commit,
      state
    }, item) {
      setTimeout(() => {
        commit('pushCarreer',  item)
      }, 1000)
    }
  },
  modules: {},
  getters: {
    getUser: state => {
      return state.user
    },
    getCarreer: state => {
      return state.carreers
    },
    getList: state => {
      return state.list
    }
  }
})
